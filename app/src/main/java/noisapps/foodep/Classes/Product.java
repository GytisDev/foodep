package noisapps.foodep.Classes;

import java.io.Serializable;

/**
 * Created by Gytis on 2018-11-27.
 */

public class Product implements Serializable {
    String name;
    int amount;
    String amountType;

    public Product(String name, int amount, String amountType){
        this.name = name;
        this.amount = amount;
        this.amountType = amountType;
    }

    public String getName() {
        return name;
    }

    public int getAmount() {
        return amount;
    }

    public String getAmountType() {
        return amountType;
    }
}
