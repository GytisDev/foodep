package noisapps.foodep.Classes;

import java.io.Serializable;

/**
 * Created by Gytis on 2018-11-27.
 */

public class Step implements Serializable {
    String description;

    public Step(String desc){
        this.description = desc;
    }

    public String GetDescription(){
        return description;
    }
}
