package noisapps.foodep.Classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gytis on 2018-11-27.
 */

public class Recipe implements Serializable {
    private String name;
    private String photoPath;
    private String description;
    private int servings;
    private int timeToMake;
    private int idInDb;
    private String type;
    private List<String> Steps;
    private List<Product> Products;

    public Recipe(String name, String photoPath, String description, String type, int servings, int timeToMake){
        this.name = name;
        this.photoPath = photoPath;
        this.description = description;
        this.servings = servings;
        this.timeToMake = timeToMake;
        this.type = type;
        Steps = new ArrayList<>();
        Products = new ArrayList<>();
        idInDb = -1;
    }

    public Recipe(String name, String photoPath, String description, String type, int servings, int timeToMake, int ID){
        this.name = name;
        this.photoPath = photoPath;
        this.description = description;
        this.servings = servings;
        this.timeToMake = timeToMake;
        this.type = type;
        Steps = new ArrayList<>();
        Products = new ArrayList<>();
        idInDb = ID;
    }

    public String getName(){
        return name;
    }

    public String getPhotoPath(){
        return photoPath;
    }

    public String getDescription(){
        return description;
    }

    public int getServings(){
        return servings;
    }

    public String getType(){
        return type;
    }

    public int getProductCount(){
        return Products.size();
    }

    public void setID(int id){
        idInDb = id;
    }

    public int getTimeToMake(){
        return timeToMake;
    }

    public void addProduct(Product product){

        for(Product prod : Products){
            if(prod.getName().toLowerCase().equals(product.getName().toLowerCase()))
                return;
        }

        Products.add(product);
    }

    public void addStep(String step){
        Steps.add(step);
    }

    public List<String> getSteps(){
        return Steps;
    }

    public int getID(){
        return idInDb;
    }

    public List<Product> getProducts(){
        return Products;
    }

    public boolean validate(){
        if(name.length() == 0){
            return false;
        } else if(Steps.size() == 0){
            return false;
        } else if(Products.size() == 0){
            return false;
        } else if(description.length() == 0){
            return false;
        } else if (timeToMake <= 0){
            return false;
        } else if (servings <= 0){
            return false;
        } else if (type.length() <= 0){
            return false;
        }

        return true;

    }
}
