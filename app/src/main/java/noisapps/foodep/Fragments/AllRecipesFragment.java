package noisapps.foodep.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

import noisapps.foodep.Activities.RecipeActivity;
import noisapps.foodep.Adapters.RecipesListAdapter;
import noisapps.foodep.Classes.Recipe;
import noisapps.foodep.Database.DatabaseHelper;
import noisapps.foodep.R;

/**
 * Created by Gytis on 2018-09-21.
 */

public class AllRecipesFragment extends Fragment {

    View rootView;
    DatabaseHelper dbHelper;
    ListView recipesList;
    Spinner typeDropdown;
    TextView messageTxt;
    CheckBox myOnlyCheckbox;

    List<Recipe> Recipes;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_all_recipes, null);

        initialize();

        return rootView;
    }

    private void initialize(){
        dbHelper = new DatabaseHelper(getContext());
        recipesList = (ListView)rootView.findViewById(R.id.lst_recipes);
        typeDropdown = (Spinner) rootView.findViewById(R.id.drp_type);
        messageTxt = (TextView) rootView.findViewById(R.id.txt_message);
        myOnlyCheckbox = (CheckBox) rootView.findViewById(R.id.checkbox_myproducts);

        typeDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                displayRecipes(typeDropdown.getSelectedItem().toString(), myOnlyCheckbox.isChecked());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });

        recipesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent nextActivity = new Intent(getActivity(), RecipeActivity.class);
                nextActivity.putExtra("recipe", Recipes.get(position));

                startActivity(nextActivity);
            }
        });

        myOnlyCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                displayRecipes(typeDropdown.getSelectedItem().toString(), isChecked);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        displayRecipes(typeDropdown.getSelectedItem().toString(), myOnlyCheckbox.isChecked());

    }

    public void displayRecipes(String type, boolean myProductsOnly){
        Recipes = dbHelper.getList(type, getString(R.string.all), myProductsOnly);

        ListAdapter adapter = new RecipesListAdapter(getContext(), Recipes.toArray(new Recipe[0]));
        recipesList.setAdapter(adapter);

        if(Recipes.size() > 0) {
            messageTxt.setVisibility(View.GONE);
        } else {
            messageTxt.setVisibility(View.VISIBLE);
        }
    }
}
