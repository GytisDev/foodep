package noisapps.foodep.Fragments;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import noisapps.foodep.Activities.RecipeActivity;
import noisapps.foodep.Classes.Product;
import noisapps.foodep.Classes.Recipe;
import noisapps.foodep.Database.DatabaseHelper;
import noisapps.foodep.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Gytis on 2018-09-21.
 */


public class AddRecipeFragment extends Fragment implements View.OnClickListener {

    static final int REQUEST_IMAGE_CAPTURE = 1;
    private String mCurrentPhotoPath;
    private ImageView mTakenPhoto;
    private Button takePhotoButton, addRecipeButton;
    private String TAG = "FOODEP Debug";
    private Recipe recipe;

    private List<View> Steps;
    private List<View> Ingredients;

    private DatabaseHelper dbHelper;

    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    View rootView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        Steps = new ArrayList<>();
        Ingredients = new ArrayList<>();

        rootView = inflater.inflate(R.layout.fragment_add_recipe, null);
        mTakenPhoto = (ImageView) rootView.findViewById(R.id.img_taken_photo);

        mCurrentPhotoPath = "";

        dbHelper = new DatabaseHelper(getContext());

        //Add listeners to buttons
        Button b = (Button) rootView.findViewById(R.id.btn_add_ingredient);
        b.setOnClickListener(this);

        Button b2 = (Button) rootView.findViewById(R.id.btn_add_step);
        b2.setOnClickListener(this);

        takePhotoButton = (Button) rootView.findViewById(R.id.btn_add_photo);
        takePhotoButton.setOnClickListener(this);

        addRecipeButton = (Button) rootView.findViewById(R.id.btn_add_recipe);
        addRecipeButton.setOnClickListener(this);
        //----------------------------------------------------------------------------

        return rootView;
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){

            case R.id.btn_add_recipe:
                if(addRecipe() != -1){
                    Toast.makeText(getActivity(), "Recipe saved!",
                            Toast.LENGTH_LONG).show();

                    Intent nextActivity = new Intent(getActivity(), RecipeActivity.class);
                    nextActivity.putExtra("recipe", recipe);

                    startActivity(nextActivity);

                } else {
                    Toast.makeText(getActivity(), "Please recheck all input.\nAll fields must be not empty.",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.btn_add_ingredient:
                addIngredient();
                break;
            case R.id.btn_add_step:
                addStep();
                break;
            case R.id.btn_add_photo:
                if(getActivity().getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY)) {
                    verifyStoragePermissions();
                    takePhoto();
                }
                else {
                    Toast.makeText(getActivity(), "No camera was found on this device.",
                            Toast.LENGTH_LONG).show();
                }
                break;
            default:
                Log.i(TAG, "Default");
                break;
        }
    }

    private int addRecipe(){

        EditText recipeNameEdt = (EditText) rootView.findViewById(R.id.edt_recipe_name);
        String name = recipeNameEdt.getText().toString();

        EditText recipeDescEdt = (EditText) rootView.findViewById(R.id.edt_recipe_description);
        String description = recipeDescEdt.getText().toString();

        Spinner typeDropDown = (Spinner) rootView.findViewById(R.id.drp_type);
        String type = typeDropDown.getSelectedItem().toString();

        EditText servingsEdt = (EditText) rootView.findViewById(R.id.edt_servings);
        EditText makeTimeEdt = (EditText) rootView.findViewById(R.id.edt_make_time);
        int servings = 0;
        int timeToMake = 0;
        try {
            servings = Integer.parseInt(servingsEdt.getText().toString());
            timeToMake = Integer.parseInt(makeTimeEdt.getText().toString());
        } catch (NumberFormatException e){
            //Bus naudojamas 0
        }

        recipe = new Recipe(name, mCurrentPhotoPath, description, type, servings, timeToMake);

        for(View view : Steps){
            EditText stepDescEdt = (EditText)view.findViewById(R.id.edt_step_desc);
            String ingName = stepDescEdt.getText().toString();

            if(ingName.length() > 0)
                recipe.addStep(ingName);

        }

        for(View view : Ingredients){
            EditText ingredientNameEdt = (EditText)view.findViewById(R.id.edt_ingredient_name);
            String ingredientName = ingredientNameEdt.getText().toString();

            if(ingredientName.length() == 0)
                continue;

            EditText ingredientAmountEdt = (EditText)view.findViewById(R.id.edt_ingredient_amount);
            int ingredientAmount = 0;
            try {
                ingredientAmount = Integer.parseInt(ingredientAmountEdt.getText().toString());
            } catch (NumberFormatException e){
                //Palikti 0
            }

            Spinner typeIngDropDown = (Spinner)view.findViewById(R.id.type_dropdown);
            String typeIng = typeIngDropDown.getSelectedItem().toString();

            recipe.addProduct(new Product(ingredientName, ingredientAmount, typeIng));

        }

        if(recipe.validate()){

            //Add to the database
            int id = dbHelper.addRecipe(recipe);
            recipe.setID(id);
            return id;

        }

        return -1;

    }

    private void takePhoto(){
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        Log.i(TAG, "Intent pasieke");
        if(cameraIntent.resolveActivity(getActivity().getPackageManager()) != null){
            File photoFile = null;
            try {
                Log.i(TAG, "create image file");
                photoFile = createImageFile();
            } catch (IOException ex){
                Log.i(TAG, ex.getMessage());
            }

            if(photoFile != null){
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(cameraIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
        Log.i(TAG, "take photo galas");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK){
            try {
                //mImageBitmap = processBitmap(MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), Uri.parse(mCurrentPhotoPath)));
                Picasso.with(getContext()).load(new File(mCurrentPhotoPath)).fit().centerCrop().into(mTakenPhoto);
                mTakenPhoto.setVisibility(View.VISIBLE);
                takePhotoButton.setVisibility(View.INVISIBLE);

            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyymmdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
          imageFileName, ".jpg", storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void addStep(){
        LinearLayout layout = (LinearLayout)rootView.findViewById(R.id.lnr_steps);
        View child = LayoutInflater.from(this.getActivity()).inflate(R.layout.item_step, null);
        child.findViewById(R.id.edt_step_desc).requestFocus();
        layout.addView(child);
        Steps.add(child);
    }

    private void addIngredient(){
        LinearLayout layout = (LinearLayout)rootView.findViewById(R.id.lnr_ingredients);
        View child = LayoutInflater.from(this.getActivity()).inflate(R.layout.item_ingredient, null);
        child.findViewById(R.id.edt_ingredient_name).requestFocus();
        layout.addView(child);
        Ingredients.add(child);
    }

    private void verifyStoragePermissions(){
        int permission = ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if(permission !=  PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(), PERMISSIONS_STORAGE, REQUEST_EXTERNAL_STORAGE);
        }
    }

}
