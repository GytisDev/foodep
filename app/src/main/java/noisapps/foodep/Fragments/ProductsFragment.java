package noisapps.foodep.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import noisapps.foodep.Adapters.ProductsListAdapter;
import noisapps.foodep.Database.DatabaseHelper;
import noisapps.foodep.R;

/**
 * Created by Gytis on 2018-09-21.
 */

public class ProductsFragment extends Fragment implements View.OnClickListener{

    ListView productsListView;
    EditText productEdt;
    List<String> products;
    ListAdapter adapter;
    DatabaseHelper dbHelper;
    int i = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_products, null);
        dbHelper = new DatabaseHelper(getContext());
        products = new ArrayList<String>();

        Button b = (Button) rootView.findViewById(R.id.btn_add_product);
        b.setOnClickListener(this);

        ImageButton b2 = (ImageButton) rootView.findViewById(R.id.btn_info);
        b2.setOnClickListener(this);

        productsListView = (ListView) rootView.findViewById(R.id.products_list);
        displayProducts();

        productEdt = (EditText) rootView.findViewById(R.id.edt_new_product);

        productsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                TextView nameTxt = (TextView) view.findViewById(R.id.txt_product);
                if(dbHelper.removeMyProduct(nameTxt.getText().toString())){
                    displayProducts();
                }
            }
        });

        return rootView;
    }

    public void displayProducts(){
        products = dbHelper.getMyProductsList();
        adapter = new ProductsListAdapter(this.getActivity(), products.toArray(new String[0]));
        productsListView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v){
        switch(v.getId()){
            case R.id.btn_add_product:
                String product = productEdt.getText().toString();
                if(product.length() > 0) {
                    if (dbHelper.addMyProduct(product)) {
                        displayProducts();
                        productEdt.setText("");
                    } else {
                        Toast.makeText(getActivity(), getString(R.string.product_exist),
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.input_field_empty),
                            Toast.LENGTH_LONG).show();
                }

                break;
            case R.id.btn_info:
                Toast.makeText(getActivity(), getString(R.string.info_product),
                        Toast.LENGTH_LONG).show();
                break;
        }
    }
}
