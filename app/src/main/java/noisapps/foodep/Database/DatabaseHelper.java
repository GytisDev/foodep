package noisapps.foodep.Database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import noisapps.foodep.Classes.Product;
import noisapps.foodep.Classes.Recipe;

/**
 * Created by Gytis on 2018-11-27.
 */

public class DatabaseHelper extends SQLiteOpenHelper {//extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "food.db";

    //Recipes
    private static final String TABLE_NAME_RECIPES = "recipes";
    private static final String RECIPES_COL_ID = "ID";
    private static final String RECIPES_COL_NAME = "NAME";
    private static final String RECIPES_COL_PHOTO = "PHOTO";
    private static final String RECIPES_COL_DESC = "DESCRIPTION";
    private static final String RECIPES_COL_SERVINGS = "SERVINGS";
    private static final String RECIPES_COL_TIMETOMAKE = "TIMETOMAKE";
    private static final String RECIPES_COL_TYPE = "TYPE";
    //Products
    private static final String TABLE_NAME_PRODUCTS = "products";
    private static final String PRODUCTS_COL_ID = "ID";
    private static final String PRODUCTS_COL_NAME = "NAME";
    private static final String PRODUCTS_COL_AMOUNT = "AMOUNT";
    private static final String PRODUCTS_COL_AMOUNTTYPE = "AMOUNTTYPE";
    private static final String PRODUCTS_COL_RECIPEID = "fk_recipeID";
    //Steps
    private static final String TABLE_NAME_STEPS = "steps";
    private static final String STEPS_COL_ID = "ID";
    private static final String STEPS_COL_DESC = "DESCRIPTION";
    private static final String STEPS_COL_RECIPEID = "fk_recipeID";
    //My products
    private static final String TABLE_NAME_MYPRODUCTS = "myproducts";
    private static final String MYPRODUCTS_COL_ID = "ID";
    private static final String MYPRODUCTS_COL_NAME = "NAME";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 2);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLE_NAME_RECIPES + "(" + RECIPES_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + RECIPES_COL_NAME + " TEXT, " + RECIPES_COL_PHOTO + " TEXT, " + RECIPES_COL_DESC + " TEXT, " + RECIPES_COL_SERVINGS + " INTEGER, " + RECIPES_COL_TIMETOMAKE + " INTEGER, " + RECIPES_COL_TYPE + " TEXT" + ");");
        db.execSQL("CREATE TABLE " + TABLE_NAME_PRODUCTS + "(" + PRODUCTS_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + PRODUCTS_COL_NAME + " TEXT, " + PRODUCTS_COL_AMOUNT + " INTEGER, " + PRODUCTS_COL_AMOUNTTYPE + " TEXT, " + PRODUCTS_COL_RECIPEID + " INTEGER, FOREIGN KEY(" + PRODUCTS_COL_RECIPEID + ") REFERENCES " + TABLE_NAME_PRODUCTS + "(" + RECIPES_COL_ID +")" + "on delete cascade);");
        db.execSQL("CREATE TABLE " + TABLE_NAME_STEPS + "(" + STEPS_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + STEPS_COL_DESC + " INTEGER, " + STEPS_COL_RECIPEID + " INTEGER, FOREIGN KEY(" + PRODUCTS_COL_RECIPEID + ") REFERENCES " + TABLE_NAME_PRODUCTS + "(" + RECIPES_COL_ID +")" +  "on delete cascade);");
        db.execSQL("CREATE TABLE " + TABLE_NAME_MYPRODUCTS + "(" + MYPRODUCTS_COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + MYPRODUCTS_COL_NAME + " TEXT);");

        Log.w("MyApp", "Pasiekta on create");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_PRODUCTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_STEPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_RECIPES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_MYPRODUCTS);
        onCreate(db);
    }

    public int addRecipe(Recipe recipe){

        int id = -1;

        try {
            //Add recipe
            ContentValues values = new ContentValues();
            values.put(RECIPES_COL_NAME, recipe.getName());
            values.put(RECIPES_COL_DESC, recipe.getDescription());
            values.put(RECIPES_COL_SERVINGS, recipe.getServings());
            values.put(RECIPES_COL_PHOTO, recipe.getPhotoPath());
            values.put(RECIPES_COL_TIMETOMAKE, recipe.getTimeToMake());
            values.put(RECIPES_COL_TYPE, recipe.getType());

            SQLiteDatabase db = getWritableDatabase();
            id = (int) db.insertOrThrow(TABLE_NAME_RECIPES, null, values);

            //Add steps
            values.clear();
            for (String step : recipe.getSteps()) {
                values.put(STEPS_COL_DESC, step);
                values.put(STEPS_COL_RECIPEID, id);

                db.insertOrThrow(TABLE_NAME_STEPS, null, values);

                values.clear();
            }



            //Add products
            values.clear();
            for (Product product : recipe.getProducts()) {
                values.put(PRODUCTS_COL_NAME, product.getName());
                values.put(PRODUCTS_COL_AMOUNT, product.getAmount());
                values.put(PRODUCTS_COL_AMOUNTTYPE, product.getAmountType());
                values.put(PRODUCTS_COL_RECIPEID, id);

                db.insertOrThrow(TABLE_NAME_PRODUCTS, null, values);

                values.clear();
            }

            db.close();

        } catch (Exception e){
            Log.i("FOODEP DEBUG", e.getMessage());
            return -1;
        }

        return id;
    }

    public List<Recipe> getList(String typeFilter, String allFromValues, boolean onlyMyProducts){
        List<Recipe> Recipes = new ArrayList<>();
        //ArrayList<String> Steps = new ArrayList<>();
        //ArrayList<Product> Products = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor;
        String query = "";

        if(typeFilter.equals(allFromValues) && !onlyMyProducts) //Visi receptai, ne tik mano produktai
            query = "SELECT * FROM " + TABLE_NAME_RECIPES + ";";
        else if(!typeFilter.equals(allFromValues) && !onlyMyProducts)//Tik nurodyto tipo receptai, ne tik mano produktai
            query = "SELECT * FROM " + TABLE_NAME_RECIPES + " WHERE " + RECIPES_COL_TYPE + " = '" + typeFilter + "';";
        else if (typeFilter.equals(allFromValues) && onlyMyProducts){ //Visi receptai, tik mano produktai
            query = "SELECT * FROM " + TABLE_NAME_RECIPES + " WHERE NOT EXISTS (" +
                    "SELECT * FROM " + TABLE_NAME_PRODUCTS +
                    " LEFT JOIN " + TABLE_NAME_MYPRODUCTS + " ON " + TABLE_NAME_PRODUCTS + "." + PRODUCTS_COL_NAME + "=" + TABLE_NAME_MYPRODUCTS + "." + MYPRODUCTS_COL_NAME +
                    " WHERE " + TABLE_NAME_RECIPES + "." + RECIPES_COL_ID + "=" + TABLE_NAME_PRODUCTS + "." + PRODUCTS_COL_RECIPEID + " AND " + TABLE_NAME_MYPRODUCTS + "." + MYPRODUCTS_COL_ID + " IS NULL );";
        } else if (!typeFilter.equals(allFromValues) && onlyMyProducts){ //Tik nurodyto tipo receptai, tik mano produktai
            query = "SELECT * FROM " + TABLE_NAME_RECIPES + " WHERE NOT EXISTS (" +
                    "SELECT * FROM " + TABLE_NAME_PRODUCTS +
                    " LEFT JOIN " + TABLE_NAME_MYPRODUCTS + " ON " + TABLE_NAME_PRODUCTS + "." + PRODUCTS_COL_NAME + "=" + TABLE_NAME_MYPRODUCTS + "." + MYPRODUCTS_COL_NAME +
                    " WHERE " + TABLE_NAME_RECIPES + "." + RECIPES_COL_ID + "=" + TABLE_NAME_PRODUCTS + "." + PRODUCTS_COL_RECIPEID + " AND " + TABLE_NAME_MYPRODUCTS + "." + MYPRODUCTS_COL_ID + " IS NULL )" +
                    " AND " + RECIPES_COL_TYPE + "='" + typeFilter + "';";
        }

        cursor = db.rawQuery(query, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            int id = cursor.getInt(cursor.getColumnIndex(RECIPES_COL_ID));
            Log.i("FOODEP", Integer.toString(id));
            String name = cursor.getString(cursor.getColumnIndex(RECIPES_COL_NAME));
            String description = cursor.getString(cursor.getColumnIndex(RECIPES_COL_DESC));
            String photoPath = cursor.getString(cursor.getColumnIndex(RECIPES_COL_PHOTO));
            int servings = cursor.getInt(cursor.getColumnIndex(RECIPES_COL_SERVINGS));
            int timeToMake = cursor.getInt(cursor.getColumnIndex(RECIPES_COL_TIMETOMAKE));
            String type = cursor.getString(cursor.getColumnIndex(RECIPES_COL_TYPE));

            Recipe recipe = new Recipe(name, photoPath, description, type, servings, timeToMake, id);

            Cursor utilCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_STEPS + " WHERE " + STEPS_COL_RECIPEID + "=" + id, null);
            utilCursor.moveToFirst();

            while(!utilCursor.isAfterLast()){
                recipe.addStep(utilCursor.getString(utilCursor.getColumnIndex(STEPS_COL_DESC)));
                utilCursor.moveToNext();
            }

            utilCursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_PRODUCTS + " WHERE " + PRODUCTS_COL_RECIPEID + "=" + id, null);
            utilCursor.moveToFirst();

            while(!utilCursor.isAfterLast()){
                String ingrediantName = utilCursor.getString(utilCursor.getColumnIndex(PRODUCTS_COL_NAME));
                int amount = utilCursor.getInt(utilCursor.getColumnIndex(PRODUCTS_COL_AMOUNT));
                String amountType = utilCursor.getString(utilCursor.getColumnIndex(PRODUCTS_COL_AMOUNTTYPE));

                recipe.addProduct(new Product(ingrediantName, amount, amountType));

                utilCursor.moveToNext();
            }

            Recipes.add(recipe);

            cursor.moveToNext();

        }

        db.close();
        return Recipes;
    }

    public boolean addMyProduct(String product){
        if(product.length() > 0){

            try {

                SQLiteDatabase db = getWritableDatabase();

                Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_MYPRODUCTS + " WHERE " + MYPRODUCTS_COL_NAME + "='" + product + "';" , null);
                Log.i("FOODEP", "SELECT * FROM " + TABLE_NAME_MYPRODUCTS + " WHERE " + MYPRODUCTS_COL_NAME + "='" + product + "'; " + cursor.getCount());
                if(cursor.getCount() > 0)
                    return false;

                ContentValues values = new ContentValues();
                values.put(MYPRODUCTS_COL_NAME, product);

                db.insertOrThrow(TABLE_NAME_MYPRODUCTS, null, values);

            } catch(Exception e){
                Log.i("FOODEP", e.getMessage());
            }

            return true;

        }

        return false;

    }

    public boolean removeMyProduct(String product){

        if(product.length() > 0){
            try {
                SQLiteDatabase db = getWritableDatabase();
                db.execSQL("DELETE FROM " + TABLE_NAME_MYPRODUCTS + " WHERE " + MYPRODUCTS_COL_NAME + "='" + product + "';");
                db.close();
                return true;
            } catch (Exception e){
                Log.i("FOODEP", e.getMessage());
                return false;
            }
        }

        return false;
    }

    public List<String> getMyProductsList(){
        List<String> products = new ArrayList<>();

        SQLiteDatabase db = getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME_MYPRODUCTS, null);

        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            products.add(cursor.getString(cursor.getColumnIndex(MYPRODUCTS_COL_NAME)));
            cursor.moveToNext();
        }

        db.close();

        return products;

    }

    public void removeRecipe(int id){
        if(id != -1) {
            Log.i("FOODEP", Integer.toString(id));
            SQLiteDatabase db = getWritableDatabase();

            db.execSQL("DELETE FROM " + TABLE_NAME_RECIPES + " WHERE " + RECIPES_COL_ID + "='" + id + "';");

            db.close();
        }
    }
}
