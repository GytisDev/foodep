package noisapps.foodep.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import noisapps.foodep.R;

/**
 * Created by Gytis on 2018-10-15.
 */

public class ProductsListAdapter extends ArrayAdapter<String>{

    public ProductsListAdapter(Context context, String[] products){
        super(context, R.layout.item_product, products);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = convertView;
        if(customView == null)
            customView = inflater.inflate(R.layout.item_product, parent, false);

        TextView product = (TextView) customView.findViewById(R.id.txt_product);

        if(getItem(position) != null)
            product.setText(getItem(position));

        return customView;
    }

}
