package noisapps.foodep.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import noisapps.foodep.Classes.Recipe;
import noisapps.foodep.R;

public class RecipesListAdapter extends ArrayAdapter<Recipe> {
    public RecipesListAdapter(Context context, Recipe[] Recipes){
        super(context, R.layout.item_recipe, Recipes);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = convertView;
        if(customView == null)
            customView = inflater.inflate(R.layout.item_recipe, parent, false);

        TextView nameView = (TextView) customView.findViewById(R.id.txt_recipe_name);
        TextView dateView = (TextView) customView.findViewById(R.id.txt_time2);
        TextView descView = (TextView) customView.findViewById(R.id.txt_desc);
        TextView typeView = (TextView) customView.findViewById(R.id.txt_type);

        String path = getItem(position).getPhotoPath().replace("file:", "");
        File imgFile = new File(path);

        Log.i("FOODEP", path);

        if(imgFile.exists()) {
            ImageView photoImg = (ImageView) customView.findViewById(R.id.img_recipe_photo);
            try {
                Picasso.with(getContext()).load(imgFile).fit().centerCrop().into(photoImg);
            } catch (Exception e){

            }
        } else {
            Log.i("FOODEP", "Failas neegzistuoja");
        }

        nameView.setText(getItem(position).getName());
        dateView.setText(getItem(position).getTimeToMake() + "min");
        descView.setText(getItem(position).getDescription());
        typeView.setText(getItem(position).getType());

        return customView;
    }

}
