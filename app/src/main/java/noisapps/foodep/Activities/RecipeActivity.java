package noisapps.foodep.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import noisapps.foodep.Classes.Product;
import noisapps.foodep.Classes.Recipe;
import noisapps.foodep.Database.DatabaseHelper;
import noisapps.foodep.R;

public class RecipeActivity extends Activity implements View.OnClickListener {

    DatabaseHelper dbHelper;
    Recipe recipe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);

        Intent intent = getIntent();
        dbHelper = new DatabaseHelper(getBaseContext());

        Button revomeButton = (Button) findViewById(R.id.btn_remove_recipe);
        revomeButton.setOnClickListener(this);

        recipe = (Recipe)intent.getSerializableExtra("recipe");
        populate(recipe);

    }

    private void populate(Recipe recipe){

        Log.i("FOODEP", "Recipes number of products: " + Integer.toString(recipe.getProducts().size()));
        Log.i("FOODEP", "Recipes number of steps: " + Integer.toString(recipe.getSteps().size()));

        String path = recipe.getPhotoPath().replace("file:", "");
        File imgFile = new File(path);

        Log.i("FOODEP", path);

        if(imgFile.exists()) {
            ImageView photoImg = (ImageView) findViewById(R.id.img_main_photo);

            Picasso.with(this).load(imgFile).fit().centerCrop().into(photoImg);
        } else {
            Log.i("FOODEP", "Failas neegzistuoja");
        }

        TextView recipeNameTxt = (TextView) findViewById(R.id.txt_recipe_name);
        recipeNameTxt.setText(recipe.getName());

        TextView servingsTxt = (TextView) findViewById(R.id.txt_recipe_servings);
        servingsTxt.setText(recipe.getServings() + " " + getString(R.string.servings));

        LinearLayout layoutIng = (LinearLayout)findViewById(R.id.lnr_ingredients_recipe);
        LinearLayout layoutSteps = (LinearLayout)findViewById(R.id.lnr_steps_static);

        for(Product product : recipe.getProducts()){
            View child = LayoutInflater.from(this).inflate(R.layout.item_ingredient_static, null);

            TextView stepTxt = (TextView)child.findViewById(R.id.txt_ingredient_name);
            stepTxt.setText(product.getName());

            TextView amountTxt = (TextView)child.findViewById(R.id.txt_ingredient_amount);
            amountTxt.setText(product.getAmount() + product.getAmountType());

            layoutIng.addView(child);
        }

        for(String step : recipe.getSteps()){
            View child = LayoutInflater.from(this).inflate(R.layout.item_step_static, null);
            TextView stepTxt = (TextView)child.findViewById(R.id.txt_step_desc);
            stepTxt.setText(step);
            layoutSteps.addView(child);
        }

    }

    @Override
    public void onClick(View v){
        switch(v.getId()){

            case R.id.btn_remove_recipe:
                showAlert();
                break;

        }
    }

    private void showAlert(){
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(getString(R.string.remove_recipe))
                .setMessage(getString(R.string.alert_remove))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        dbHelper.removeRecipe(recipe.getID());
                        removePhoto();
                        finish();
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void removePhoto(){
        File file = new File(recipe.getPhotoPath());
        if(file.exists()){
            file.delete();
        }
    }


}
