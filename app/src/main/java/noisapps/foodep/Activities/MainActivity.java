package noisapps.foodep.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import noisapps.foodep.Fragments.AddRecipeFragment;
import noisapps.foodep.Fragments.AllRecipesFragment;
import noisapps.foodep.Fragments.ProductsFragment;
import noisapps.foodep.R;

public class MainActivity extends AppCompatActivity
        implements BottomNavigationView.OnNavigationItemSelectedListener {

    Fragment fragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        loadFragment(new AllRecipesFragment());
    }

    public boolean loadFragment(Fragment fragment){
        if(fragment != null){

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();

            return true;
        }

        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        fragment = null;

        switch(item.getItemId()){

            case R.id.navigation_all_recipes:
                fragment = new AllRecipesFragment();
                break;
            case R.id.navigation_add_recipe:
                fragment = new AddRecipeFragment();
                break;
            case R.id.navigation_products:
                fragment = new ProductsFragment();
                break;

        }

        return loadFragment(fragment);

    }

}
